# https://www.keycdn.com/support/apache-access-log
# https://stackoverflow.com/questions/62905708/insert-into-statement-python-regex-and-sqlite
# !/usr/bin/env python3

import sqlite3
import re

log_file = "D:/ssh_keys/cowrie/access.log"
sqlite_db_file = "access.db"

# Regular Expressions (regex)
client_ip = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"  # format ex. xxx.xxx.xxx.xxx
date = r"(3[01]|[12][0-9]|0?[1-9])\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/((?:19|20)\d{2})"  # format ex. dd/Oct/yyyy
time = r"(?:[01]\d|2[0123]):(?:[012345]\d):(?:[012345]\d)"  # format ex. hh:mm:ss
req_methods = r"(GET|HEAD|POST|PUT|DELETE|CONNECT|OPTIONS|TRACE|PATCH)"  # https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
stat_code = r"(100|101|102|200|201|202|203|204|205|206|207|208|226|300|301|302|303|304|305|306|307|308" \
                 r"|400|401|402|403|404|405|406|407|408|409|410|411|412|413|414|415|416|417|418|420|422|426|428|429|431|444|449|450|451|499" \
                 r"|500|501|502|503|504|505|506|507|508|509|510|511|598|599)" #https://www.restapitutorial.com/httpstatuscodes.html

# Setup of sqlite database
conn = sqlite3.connect(sqlite_db_file)
c = conn.cursor()

c.execute("""CREATE TABLE details ('client_ip' text) """)
conn.commit()

pattern = re.compile(client_ip)

with open(log_file, "r") as file:
    for line in file:
        matches = pattern.findall(line)
        for match in matches:
            c.execute("""INSERT INTO details (client_ip) VALUES (?)""", (match,))
            conn.commit()  # this commits current transaction

conn.close()

# match_list = []
# with open(log_file, "r") as file:
#     for line in file:
#         for match in re.finditer(stat_code, line, re.S):
#             match_text = match.group()
#             match_list.append(match_text)
#             #print(match_text)
