# https://www.keycdn.com/support/apache-access-log
# https://stackoverflow.com/questions/62905708/insert-into-statement-python-regex-and-sqlite
# !/usr/bin/env python3

import sqlite3
import re

log_file = "D:/ssh_keys/cowrie/access.log"
sqlite_db_file = "access.db"

# Regular Expressions (regex)
client_ip = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} -"  # format ex. xxx.xxx.xxx.xxx -
date = r"(3[01]|[12][0-9]|0?[1-9])\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/((?:19|20)\d{2})"  # format ex. dd/Oct/yyyy
time = r"(?:[01]\d|2[0123]):(?:[012345]\d):(?:[012345]\d)"  # format ex. hh:mm:ss
req_methods = r"(GET|HEAD|POST|PUT|DELETE|CONNECT|OPTIONS|TRACE|PATCH)"  # https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
stat_code = r"(200|201|301|404)"  # https://www.restapitutorial.com/httpstatuscodes.html

# Setup of sqlite database
conn = sqlite3.connect(sqlite_db_file)
c = conn.cursor()
match_list = []

c.execute("""CREATE TABLE details ('client_ip' text, 'date' text, 'time' text, 'req_methods' text, 'stat_code' text) """)
conn.commit()

with open(log_file, "r") as file:
    for line in file:
        # client_ip
        for match in re.finditer(client_ip, line, re.S):
            match_text1 = match.group()
            match_list.append(match_text1)

        # date
        for match in re.finditer(date, line, re.S):
            match_text2 = match.group()
            match_list.append(match_text2)

        # time
        for match in re.finditer(time, line, re.S):
            match_text3 = match.group()
            match_list.append(match_text3)

        # req_methods
        for match in re.finditer(req_methods, line, re.S):
            match_text4 = match.group()
            match_list.append(match_text4)

        # stat_code
        for match in re.finditer(stat_code, line, re.S):
            match_text5 = match.group()
            match_list.append(match_text5)

            c.execute("""INSERT INTO details (client_ip, date, time, req_methods, stat_code) VALUES (?,?,?,?,?)""",
                      (match_text1, match_text2, match_text3, match_text4, match_text5,))
            conn.commit()  # this commits current transaction

conn.close()
