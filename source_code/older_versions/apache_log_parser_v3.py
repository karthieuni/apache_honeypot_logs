# https://www.keycdn.com/support/apache-access-log
# https://www.poftut.com/understanding-configuring-apache-access-log/
# https://stackoverflow.com/questions/62905708/insert-into-statement-python-regex-and-sqlite
# https://www.youtube.com/watch?v=vshqLskHXYY
# !/usr/bin/env python3

import sqlite3
import re

# File path
apache_log = "D:/ssh_keys/apache_cowrie_logs/logs/access.log"
sqlite_db = "D:/ssh_keys/apache_cowrie_logs/apache.db"

# Regular Expressions (regex)
# client_ip = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} -"  # format ex. xxx.xxx.xxx.xxx - (Error with dash "-")
date = r"(3[01]|[12][0-9]|0?[1-9])\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/((?:19|20)\d{2})"  # format ex. dd/Oct/yyyy
# time = r"((?:[012345]\d):(?:[012345]\d):(?:[012345]\d):(?:[012345]\d))"  # format ex. hh:mm:ss - (Error year (yy) with the time)
req_methods = r"(GET|HEAD|POST|PUT|DELETE|CONNECT|OPTIONS|TRACE|PATCH|PROPFIND)"  # https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
stat_code = r"(200|201|301|302|404|500)"  # https://www.restapitutorial.com/httpstatuscodes.html
http_version = r"HTTP/1.0|HTTP/1.1|HTTP/1.2|HTTP/1.3|HTTP/2.0"
user_agent = r" \"(.+?)\""

# Connect to the sqlite database
conn = sqlite3.connect(sqlite_db)
c = conn.cursor()

# Create a table
c.execute("""CREATE TABLE apache_log (
                 'id' INTEGER PRIMARY KEY,
                 'client_ip' text,
                 'date' text,
                 'time' text,
                 'time_zone' text,
                 'req_methods' text,
                 'stat_code' text,
                 'byte_size' text,
                 'http_version' text,
                 'user_agent' text,
                 'url' text) """)
conn.commit()  # This commits current transaction

match_list = []  # Create a list

with open(apache_log, "r") as file:  # Extract and read data from the file
    for line in file:  # Read lines in the file
        # client_ip
        match_text1 = line.split(' - - ')[0]  # Using split to find IP addresses

        # date
        for match in re.finditer(date, line, re.S):
            match_text2 = match.group()  # Regex
            match_list.append(match_text2)

        # time
        match_text3 = line.split(' ]')[0].split('[')[1].split('2020')[1][1:9]  # HH:MM:SS

        # time_zone
        match_text4 = line.split(' ]')[0].split('[')[1].split()[1][0:5]

        # req_methods
        for match in re.finditer(req_methods, line, re.S):
            match_text5 = match.group()  # Regex
            match_list.append(match_text5)

        # stat_code
        for match in re.finditer(stat_code, line, re.S):
            match_text6 = match.group()  # Regex
            match_list.append(match_text6)

        # byte_size - Size of the resource that was requested
        try:
            match_text7 = line.split('"-"')[0].split('HTTP')[1].split(' ')[2]
        except:
            print("Bypassing error (byte_size): some invalid request 400 0")

        # http_version
        for match in re.finditer(http_version, line, re.S):
            match_text8 = match.group()  # Regex
            match_list.append(match_text8)

        # user_agent
        for match in re.finditer(user_agent, line, re.S):
            match_text9 = match.group()  # Regex
            match_list.append(match_text9)
        # match_text9 = line.split(' ]')[0].split('[')[1].split('-" "')[-1] # Another way to do it

        # urls (html, php, json, api)
        try:
            match_text10 = line.split(' H')[0].split('"')[1].split(' ')[1]
        except:
            print("Bypassing error (urls): unknown urls")

        # Insert the data into the table
        c.execute("""INSERT INTO apache_log (client_ip, date, time, time_zone, req_methods, stat_code, byte_size,
        http_version, user_agent, url) VALUES (?,?,?,?,?,?,?,?,?,?)""", (match_text1, match_text2, match_text3,
                                                                         match_text4, match_text5, match_text6,
                                                                         match_text7, match_text8, match_text9,
                                                                         match_text10,))
        conn.commit()  # This commits current transaction

conn.close()  # sqlite database - connection closed
