# https://www.keycdn.com/support/apache-access-log
# https://www.poftut.com/understanding-configuring-apache-access-log/
# https://stackoverflow.com/questions/62905708/insert-into-statement-python-regex-and-sqlite
# https://www.youtube.com/watch?v=vshqLskHXYY
# https://github.com/maxmind/GeoIP2-python
# https://etiennemunnich.github.io/working-with-maxmind-geoip/
# https://stackoverflow.com/questions/11901010/reverse-dns-lookup-in-python

# !/usr/bin/env python3
import geoip2.database
import sqlite3
import socket
import re
print("|----------------IMPORTANT NOTICE------------------|")
print("Connect to the internet - To get passive DNS results")
print("This script may take some time !!!")

# File path
apache_log = "C:/Users/Karthie/Desktop/logs/ec2_instance_1/apache_log/4in1_access.log"
sqlite_db = "C:/Users/Karthie/Desktop/logs/ec2_instance_1/database_files/4in1_access.db"

# GeoIP database paths
asn_reader = "D:/ssh_keys/apache_cowrie_logs/geoip_packages/GeoLite2-ASN.mmdb"
city_reader = "D:/ssh_keys/apache_cowrie_logs/geoip_packages/GeoLite2-City.mmdb"
country_reader = "D:/ssh_keys/apache_cowrie_logs/geoip_packages/GeoLite2-Country.mmdb"

# Regular Expressions (regex)
# client_ip = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"  # format ex. xxx.xxx.xxx.xxx - (Error with dash "-")
date = r"(3[01]|[12][0-9]|0?[1-9])\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\/((?:19|20)\d{2})"  # format ex. dd/Oct/yyyy
# time = r"((?:[012345]\d):(?:[012345]\d):(?:[012345]\d):(?:[012345]\d))"  # format ex. hh:mm:ss - (Error year (yy) with the time)
req_methods = r"(GET|HEAD|POST|PUT|DELETE|CONNECT|OPTIONS|TRACE|PATCH|PROPFIND)"  # https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
stat_code = r"(200|201|301|302|400|404|500)"  # https://www.restapitutorial.com/httpstatuscodes.html
http_version = r"HTTP/1.0|HTTP/1.1|HTTP/1.2|HTTP/1.3|HTTP/2.0"
user_agent = r" \"(.+?)\""

# Connect to the sqlite database
conn = sqlite3.connect(sqlite_db)
c = conn.cursor()

# Create a table
c.execute("""CREATE TABLE apache_log (
                 'id' INTEGER PRIMARY KEY,
                 'client_ip' float,
                 'date' int,
                 'time' int,
                 'time_zone' text,
                 'req_methods' text,
                 'stat_code' int,
                 'byte_size' int,
                 'http_version' text,
                 'user_agent' text,
                 'url' text) """)

conn.commit()  # This commits current transaction

c.execute("""CREATE TABLE enrichment (
                 'id' INTEGER PRIMARY KEY,
                 'asn' int,
                 'country' text,
                 'city' text,
                 'post_code' int,
                 'latitude' float,
                 'longitude' float,
                 'passive_dns' float) """)

conn.commit()  # This commits current transaction

match_list = []  # Create a list

with open(apache_log, "r") as file:  # Extract and read data from the file
    for line in file:  # Read lines in the file
        # client_ip
        try:
            match_text1 = line.split(' - - ')[0]  # Using split to find IP addresses
        except:
            print("Bypassing errors with client ip")

        # date
        try:
            for match in re.finditer(date, line, re.S):
                match_text2 = match.group()  # Regex
                match_list.append(match_text2)
        except:
            print("Bypassing errors with date")

        # time
        try:
            match_text3 = line.split(' ]')[0].split('[')[1].split('2020')[1][1:9]  # HH:MM:SS
        except:
            print("Bypassing errors with time")

        # time_zone
        try:
            match_text4 = line.split(' ]')[0].split('[')[1].split()[1][0:5]
        except:
            print("Bypassing errors with time zone")

        # req_methods
        try:
            for match in re.finditer(req_methods, line, re.S):
                match_text5 = match.group()  # Regex
                match_list.append(match_text5)
        except:
            print("Bypassing errors with the request methods")

        # stat_code
        try:
            for match in re.finditer(stat_code, line, re.S):
                match_text6 = match.group()  # Regex
                match_list.append(match_text6)
        except:
            print("Bypassing error with status code")

        # byte_size - Size of the resource that was requested
        try:
            match_text7 = line.split('"-"')[0].split('HTTP')[1].split(' ')[2]
        except:
            print("Bypassing error (byte_size): some invalid request 400 0")

        # http_version
        try:
            for match in re.finditer(http_version, line, re.S):
                match_text8 = match.group()  # Regex
                match_list.append(match_text8)
        except:
            print("Bypassing error with http version")

        # user_agent
        try:
            for match in re.finditer(user_agent, line, re.S):
                match_text9 = match.group()  # Regex
                match_list.append(match_text9)
                # match_text9 = line.split(' ]')[0].split('[')[1].split('-" "')[-1] # Another way to do it
        except:
            print("Bypassing error with user_agent")

        # urls (html, php, json, api)
        try:
            match_text10 = line.split(' H')[0].split('"')[1].split(' ')[1]
        except:
            print("Bypassing error (urls): unknown urls")

        # ASN number
        with geoip2.database.Reader(asn_reader) as reader:
            response = reader.asn(match_text1)
            asn = response.autonomous_system_number

        # GeoIP data - City
        with geoip2.database.Reader(city_reader) as reader:
            response = reader.city(match_text1)
            city = response.city.name
            latitude = response.location.latitude
            longitude = response.location.longitude
            post_code = response.postal.code

        # GeoIP data - Country
        with geoip2.database.Reader(country_reader) as reader:
            response = reader.country(match_text1)
            # country_code = response.country.iso_code
            country = response.country.name

        # Adding Passive DNS
        try:
            passive_dns = socket.getnameinfo((match_text1, 0), 0)[0]
        except:
            print("Bypassing errors with passive dns")

        # Insert the data into the table - apache.log
        c.execute("""INSERT INTO apache_log (client_ip, date, time, time_zone, req_methods, stat_code, byte_size, 
        http_version, user_agent, url) VALUES (?,?,?,?,?,?,?,?,?,?)""", (match_text1, match_text2, match_text3,
                                                                         match_text4, match_text5, match_text6,
                                                                         match_text7, match_text8, match_text9,
                                                                         match_text10,))
        conn.commit()  # This commits current transaction

        # Insert the data into the table - enrichment
        c.execute("""INSERT INTO enrichment (asn, country, city, post_code, latitude, longitude, passive_dns)
        VALUES (?,?,?,?,?,?,?)""", (asn, country, city, post_code, latitude, longitude, passive_dns,))
        conn.commit()  # This commits current transaction

print("|------------------------------------------------------------------------------|")
print("Database has been successfully created")
print("Find the directory db file path here: " + sqlite_db)
print("Open the program 'DB Browser (SQLite) program', drag and drop the db file.")
conn.close()  # connection closed
