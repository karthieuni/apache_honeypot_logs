# https://www.youtube.com/watch?v=A-mjWOE9Mcc
# https://www.geeksforgeeks.org/extract-ip-address-from-file-using-python/
# https://virustotal.github.io/vt-py/quickstart.html#get-information-about-an-url
# Script checks if the site is harmless, malicious, suspicious, timeout, undetected
# CAN ONLY GET REPORT FOR 1 IP ADDRESS
# REMEMBER: Total no. of requests is limited
# !/usr/bin/env python3

import vt

api_key = ''  # api_key from virustotal.com
client = vt.Client(api_key)
url_id = vt.url_id("4dmobil.at")
url = client.get_object("/urls/{}", url_id)
print(url.last_analysis_stats)

client.close()
