# https://github.com/PaulSec/API-dnsdumpster.com
# Script takes 1 IP address then does a reverse lookup and finds all the necessary DNS info.

from dnsdumpster.DNSDumpsterAPI import DNSDumpsterAPI
import socket

ip_address = "8.8.8.8"
reverse_lookup = socket.gethostbyaddr(ip_address)[0]
domain = reverse_lookup

resource = DNSDumpsterAPI(True).search(domain)

# Domain
print("\nDomain: " + resource['domain'])

# DNS Name Servers
print("\nDNS Name Servers:")
for entry in resource['dns_records']['dns']:
    nameservers = "{domain}".format(**entry)
    print(("{domain} ({ip}) {as} {provider} {country}".format(**entry)))

# MX Records
print("\nMX Records: ")
for entry in resource['dns_records']['mx']:
    print(("{domain} ({ip}) {as} {provider} {country}".format(**entry)))

# Host Records (A)
print("\nHost Records (A): ")
for entry in resource['dns_records']['host']:
    if entry['reverse_dns']:
        print(("{domain} ({reverse_dns}) ({ip}) {as} {provider} {country}".format(**entry)))
    else:
        print(("{domain} ({ip}) {as} {provider} {country}".format(**entry)))

# TXT records
print("\nTXT Records: ")
for entry in resource['dns_records']['txt']:
    print(entry)
