# https://www.youtube.com/watch?v=A-mjWOE9Mcc
# https://www.geeksforgeeks.org/extract-ip-address-from-file-using-python/
# Script takes the apache.log, gets all the client ip addresses and puts them in a text file

import re

apache_log = "D:/ssh_keys/apache_cowrie_logs/logs/access.log"

with open(apache_log) as fh:  # opening and reading the file
    fstring = fh.readlines()

pattern = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')  # regex pattern for IP addresses

lst = []  # initializing the list object

for line in fstring:  # extracting the IP addresses
    lst.append(pattern.search(line)[0])

with open("ip_addresses.txt", "w") as file:  # place the IP addresses to a file
    file.write("\n".join(lst))

ip_addresses = "D:/ssh_keys/apache_cowrie_logs/ip_addresses.txt"